var express     =   require("express");
var user        =   require("../models/users");
var books       =   require("../models/books");
var router      =   express.Router();

//route() will allow you to use same path for different HTTP operation.
//So if you have same URL but with different HTTP OP such as POST,GET etc
//Then use route() to remove redundant code.

//------------------- Lister les utilisateurs --------------
router.route("/").get(function(req,res){
    var response = {};
    user.find({},function(err,data){
        // Mongo command to fetch all data from collection.
        if(err) {
            response = {"error" : true,"message" : "Error fetching data"};
        } else {
            response = {"error" : false,"message" : data};
        }
        res.json(response);
    });
});
//------------------- Lister les utilisateurs --------------



//------------------- Créer un utilisateur --------------
router.route("/").post(function(req,res){
    var db = new user();
    var response = {};
    // fetch email and password from REST request.
    // Add strict validation when you use this in Production.
    db.userEmail = req.body.email;
    // Hash the password using SHA1 algorithm.
    // db.userPassword =  require('crypto')
    //     .createHash('sha1')
    //     .update(req.body.password)
    //     .digest('base64');
    db.userPassword = req.body.password;
    db.save(function(err){
        // save() will run insert() command of MongoDB.
        // it will add new data in collection.
        if(err) {
            response = {"error" : true,"message" : "Error adding data"};
        } else {
            response = {"error" : false,"message" : "Utilisateur \"" + req.body.email + "\" ajouté"};
        }
        res.json(response);
    });
});
//------------------- Créer un utilisateur --------------



//------------------- Aficher un utilisateur --------------
router.route("/:id").get(function(req,res){
    var response = {};
    user.findById(req.params.id)
    .populate('read') // only return the Book title read
    .populate('unread') // only return the Book title unread
    .exec(function(err, user){
        // This will run Mongo Query to fetch data based on ID.
        if(err) {
            response = {"error" : true, "message" : "Error fetching data"};
        } else {
            response = {"error" : false, "message" : user};
        }
        res.json(response);
    });
});
//------------------- Aficher un utilisateur --------------


//------------------- Modifier un utilisateur --------------
router.route("/:id").put(function(req,res){
    var response = {};
    // first find out record exists or not
    // if it does then update the record
    user.findById(req.params.id,function(err,data){
        if(err) {
            response = {"error" : true,"message" : "Error fetching data"};
        } else {
            // we got data from Mongo.
            // change it accordingly.
            if(req.body.userEmail !== undefined) {
                // case where email needs to be updated.
                data.userEmail = req.body.userEmail;
            }
            if(req.body.userPassword !== undefined) {
                // case where password needs to be updated
                data.userPassword = req.body.userPassword;
            }
            // save the data
            data.save(function(err){
                if(err) {
                    response = {"error" : true,"message" : "Error updating data"};
                } else {
                    response = {"error" : false,"message" : "Data is updated for "+req.params.id};
                }
                res.json(response);
            })
        }
    });
});
//------------------- Modifier un utilisateur --------------



//------------------- Supprimer un utilisateur --------------
router.route("/:id").delete(function(req,res){
    var response = {};
    // find the data
    user.findById(req.params.id,function(err,data){
        if(err) {
            response = {"error" : true,"message" : "Error fetching data"};
        } else {
            // data exists, remove it.
            user.remove({_id : req.params.id},function(err){
                if(err) {
                    response = {"error" : true,"message" : "Error deleting data"};
                } else {
                    response = {"error" : true,"message" : "Data associated with "+req.params.id+"is deleted"};
                }
                res.json(response);
            });
        }
    });
});
//------------------- Supprimer un utilisateur --------------



//------------------- Lister les livres de l'utilisateur --------------
router.route("/:id/books").get(function(req,res){
    var response = {};
    // user.findById(req.params.id,function(err,data){
    //     // This will run Mongo Query to fetch data based on ID.
    //     if(err) {
    //         response = {"error" : true,"message" : "Error fetching data"};
    //     } else {
    //         if (data.read.length === 0) {
    //             response = {"error" : false,"message" : "Pas de livre pour cet utilisateur"};
    //         } else {
    //             response = {"error" : false,"message" : data};
    //         }
    //     }
    //     res.json(response);
    // });
});
//------------------- Lister les livres de l'utilisateur --------------



//------------------- Ajouter un livre lu à l'utilisateur --------------
router.route("/:id/read/:idbook").get(function(req,res){
    var idUser = req.params.id;
    var idBook = req.params.idbook;
    user.findById(idUser,function(err,data){ // trouver l'user par son id
        if(err) {
            response = {"error" : true,"message" : "Erreur au chargement de l'utilisateur"};
        } else {
            var userInfos = data;
            books.findById(idBook,function(err,data){ // trouver le book par son id
                var bookInfos = data;
                if(err) {
                    response = {"error" : true,"message" : "Erreur au chargement du livre"};
                }
                else {
                    user.update(
                        {_id: userInfos._id},
                        {
                            $addToSet: {read: bookInfos}
                        }, //$addToSet permet de n'update que si la valeur n'existe pas déjà
                        function(err, data, numAffected) {
                            res.json(data);
                        }
                    );
                    // user.findById(idUser, function(err, data) {
                    //     var unreadBooks = data.unread;
                    //     var unreadBooksLength = data.unread.length;
                    //     console.log(unreadBooksLength);
                    //     for(var i = 0; i < unreadBooksLength; ++i) {
                    //         var x = String(unreadBooks[i]);
                    //         var y = String(bookInfos._id);
                    //         if(x == y) {
                    //             console.log('delete');
                    //             delete(unreadBooks[i]);
                    //         }
                    //     }
                    //     data.save();
                    // });
                }
            })
        }
    });
});
//------------------- Ajouter un livre lu à l'utilisateur --------------



//------------------- Ajouter un livre non lu à l'utilisateur --------------
router.route("/:id/unread/:idbook").get(function(req,res){
    var idUser = req.params.id;
    var idBook = req.params.idbook;
    user.findById(idUser,function(err,data){ // trouver l'user par son id
        if(err) {
            response = {"error" : true,"message" : "Erreur au chargement de l'utilisateur"};
        } else {
            var userInfos = data;
            books.findById(idBook,function(err,data){ // trouver le book par son id
                var bookInfos = data;
                if(err) {
                    response = {"error" : true,"message" : "Erreur au chargement du livre"};
                }
                else {
                    user.update(
                        {_id: userInfos._id},
                        {
                            $addToSet: {unread: bookInfos},
                            $unset: {read: bookInfos._id}
                        }, //$addToSet permet de n'update que si la valeur n'existe pas déjà
                        function(err, data, numAffected) {
                            res.json(data);
                        }
                    );
                    // user.findById(idUser, function(err, data) {
                    //     var readBooks = data.read;
                    //     var readBooksLength = data.read.length;
                    //     for(var i = 0; i < readBooksLength; ++i) {
                    //         var x = readBooks[i];
                    //         if(x == bookInfos._id) {
                    //             console.log('delete');
                    //             delete (x);
                    //         }
                    //     }
                    //     data.save();
                    // });
                }
            });
        }
    });
});
//------------------- Ajouter un livre non lu à l'utilisateur --------------

module.exports = router;
