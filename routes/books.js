var express     =   require("express");
var mongoOp     =   require("../models/books");
var router      =   express.Router();


//------------------- Liste des livres --------------
router.route("/").get(function(req,res){
    var response = {};
    mongoOp.find({},function(err,data){
        if(err) { response = {"error" : true,"message" : "Erreur à l'identification du livre"}; }
        else { response = {"error" : false,"message" : data}; }
        res.json(response);
    });
});
//------------------- Liste des livres --------------



//------------------- Ajouter un livre --------------
router.route("/").post(function(req,res){
    var db = new mongoOp();
    var response = {};
    // titre du livre
    db.bookTitle = req.body.title;
    // description du livre
    db.bookDescription = req.body.description;
    // code du livre
    db.bookCode = req.body.code;
    db.save(function(err){
        if(err) { response = {"error" : true,"message" : "Erreur à l'ajout de données"}; }
        else { response = {"error" : false,"message" : "Livre \"" + req.body.title + "\" ajouté"}; }
        res.json(response);
    });
});
//------------------- Ajouter un livre --------------



//------------------- Modifier un livre --------------
router.route("/:id").put(function(req,res){
    var response = {};
    // first find out record exists or not
    // if it does then update the record
    mongoOp.findById(req.params.id,function(err,data){
        if(err) {
            response = {"error" : true,"message" : "Error fetching data"};
        } else {
            // we got data from Mongo.
            // change it accordingly.
            if(req.body.title !== undefined) {
                data.bookTitle = req.body.title;
            }
            if(req.body.description !== undefined) {
                data.bookDescription = req.body.description;
            }
            if(req.body.code !== undefined) {
                data.bookCode = req.body.code;
            }
            // save the data
            data.save(function(err){
                if(err) {
                    response = {"error" : true,"message" : "Error updating data"};
                } else {
                    response = {"error" : false,"message" : "Data is updated for "+req.params.id};
                }
                res.json(response);
            })
        }
    });
});
//------------------- Modifier un livre --------------



//------------------- Supprimer un livre --------------
router.route("/:id").delete(function(req,res){
    var response = {};
    // find the data
    mongoOp.findById(req.params.id,function(err,data){
        if(err) {
            response = {"error" : true,"message" : "Erreur à l'identification du livre"};
        } else {
            // data exists, remove it.
            mongoOp.remove({_id : req.params.id},function(err){
                if(err) {
                    response = {"error" : true,"message" : "Erreur à la suppression"};
                } else {
                    response = {"error" : true,"message" : "Livre "+req.params.id+" supprimé"};
                }
                res.json(response);
            });
        }
    });
});
//------------------- Supprimer un livre --------------


module.exports = router;
