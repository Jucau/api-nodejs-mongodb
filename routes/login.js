var express     =   require("express");
var mongoOp     =   require('../models/users');
var router      =   express.Router();
var app         =   express();
var jwt         =   require('jsonwebtoken');
var config      =   require('../config');

// route to authenticate a user (POST http://localhost:8080/api/authenticate)
router.post('/', function(req, res) {
    var db = new mongoOp();
    // find the user
    mongoOp.findOne({
        userEmail: req.body.email
    }, function(err, user) {
        if (err) throw err;
        if (!user) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        }
        else if (user) {
            // check if password matches
            if (user.userPassword != req.body.password) {
                res.json({ success: false, message: 'Authentication failed. Wrong password.' });
            }
            else {
                // if user is found and password is right
                // create a token
                var token = jwt.sign(user._id, config.secret, {
                    expiresIn : 60*60*24 // expires in 24 hours
                });
                // return the information including token as JSON
                res.json({
                    success: true,
                    message: 'Enjoy your token!',
                    token: token,
                    id: user._id
                });
            }
        }
  });
});

module.exports = router;
