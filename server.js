var express     =   require("express");
var app         =   express();
var bodyParser  =   require("body-parser");
var morgan      =   require('morgan');
var mongoose    =   require("mongoose");
var router      =   express.Router();

var jwt    = require('jsonwebtoken');

var config = require('./config'); // get our config file


var port = process.env.PORT || 8080; // used to create, sign, and verify tokens
mongoose.connect(config.database); // connect to database
app.set('superSecret', config.secret); // secret variable

// use morgan to log requests to the console
app.use(morgan('dev'));


// ------------------ Models ------------------
var mongoOp     =   require("./models/users");
// ------------------ Models ------------------


// ------------- Connection DB ------------------
// create instance of Schema
var mongoSchema =   mongoose.Schema;
// ------------- Connection DB ------------------

app.use(function(req, res, next) {

    // Website you wish to allow to connect
   res.setHeader('Access-Control-Allow-Origin', '*');
   // Request methods you wish to allow
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
   // Request headers you wish to allow
   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
   // Set to true if you need the website to include cookies in the requests sent
   // to the API (e.g. in case you use sessions)
   res.setHeader('Access-Control-Allow-Credentials', true);

   next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({"extended" : false}));



// ------------- Middlewares ------------------
var apiMiddleware    =   require('./middlewares/api');
app.use('/', apiMiddleware);
// ------------- Middlewares ------------------


// ------------- Routing ------------------
var indexRoute       =   require('./routes/index');
var booksRoute       =   require('./routes/books');
var usersRoute       =   require('./routes/users');
var loginRoute       =   require('./routes/login');



app.use('/',indexRoute);
app.use('/login', loginRoute);
app.use('/users', usersRoute);
app.use('/books', booksRoute);

app.listen(3000, function(){
    console.log("Listening to PORT 3000");
});
