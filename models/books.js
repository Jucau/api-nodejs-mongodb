var mongoose    =   require("mongoose");
var Schema = mongoose.Schema;

// create schema
var bookSchema  = Schema({
    "bookTitle" : String,
    "bookDescription" : String,
    "bookCode" : String
});
// create model if not exists.
module.exports = mongoose.model('books',bookSchema);
