var mongoose    =   require("mongoose");
var Schema = mongoose.Schema;

// create schema
var userSchema  = Schema({
    "userEmail" : String,
    "userPassword" : String,
    "unread" : [
        {
            type: Schema.Types.ObjectId,
            ref: 'books'
        }
    ],
    "read" : [
        {
            type: Schema.Types.ObjectId,
            ref: 'books'
        }
    ]
});
// create model if not exists.
module.exports = mongoose.model('users',userSchema);
