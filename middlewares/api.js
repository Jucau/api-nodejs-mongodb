var express     =   require("express");
var mongoOp     =   require('../models/users');
var router      =   express.Router();
var app         =   express();
var jwt         =   require('jsonwebtoken');
var config      =   require('../config');


// route middleware to verify a token
var middleware = function(req, res, next) {

    if(req.originalUrl !== '/login') {
        // check header or url parameters or post parameters for token
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
         // decode token
         if (token) {
             // verifies secret and checks exp
             jwt.verify(token, config.secret, function(err, decoded) {
                 if (err) {
                     return res.json({ success: false, message: 'Erreur pour authentifier le token.' });
                 } else {
                     // if everything is good, save to request for use in other routes
                     req.decoded = decoded;
                 }
             });
         } else {
             // if there is no token
             // return an error
             return res.status(403).send({
                 success: false,
                 message: 'Pas de token fourni.'
             });
         }
    }

    next();
};

module.exports = middleware;
